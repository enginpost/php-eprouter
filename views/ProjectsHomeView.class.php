<?php

class ProjectsHomeView extends ep\Router\Abstracts\ViewAbstract{

  public function render( $data ){
    $output = <<< EOT
    <!doctype html>
<html>
  <head>
    <meta charset=utf-8>
    <title></title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/main.css" type="text/css" />
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Sample HTML here!</h1>
          <div class="jquery-target"></div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="//code.jquery.com/jquery-latest.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/main.js"></script>
  </body>
</html>
EOT;
    echo $output;
  }

}