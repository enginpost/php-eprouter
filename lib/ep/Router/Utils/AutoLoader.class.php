<?php  

namespace ep\Router\Utils;

class AutoLoader{

static public function loadClass( $this_classname ){
    if( strlen( $this_classname) > 0 ){
      $class_path = "";
      switch( true ){
        case preg_match( '/[a-zA-Z]+Interface$/', $this_classname ) :
          // Load the PHPRest Interface classes from the library
          $class_path = './lib/ep/Router/Interfaces/' .
                        substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) .
                        '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+Abstract$/', $this_classname ) :
          // Load the PHPRest Abstract classes from the library
          $class_path = './lib/ep/Router/Abstracts/' .
                        substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) .
                        '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+Controller$/', $this_classname ) :
          // Load any custom Controller classes from the controllers project folder
          $class_path =  './controllers/' . $this_classname . '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+Model$/', $this_classname ) :
          // Load any custom Model classes from the models project folder
          $class_path =  './models/' . $this_classname . '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+View$/', $this_classname ) :
          // Load any custom Views classes from the views project folder
          $class_path =  './views/' . $this_classname . '.class.php';
          break;
      }
      include $class_path;
    }
  }
}