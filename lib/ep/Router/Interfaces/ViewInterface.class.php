<?php

namespace ep\Router\Interfaces;

interface ViewInterface{
  public function render( $data ); // A view render function
}