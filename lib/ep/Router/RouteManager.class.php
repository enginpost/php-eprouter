<?php

namespace ep\Router;

class RouteManager{
  // private variables
  private $_route_table;
  private $_url;
  private $_path;
  private $_query_string;
  private $_verb;
  private $_view_format;
  private $_rest_params;
  private $_query_params;
  private $_route_match;

  //private methods

  private function parseParams(){
    if(  $this->checkRoutes() ){

      parse_str( $this->_query_string, $this->_query_params );
      $rest_parts = explode( "/", substr($this->_path,0,1) === "/" ? substr($this->_path,1) : $this->_path );
      $test_parts = explode("/", $this->_route_match['route'] );
      for( $i = 0; $i < count( $test_parts ); $i++ ){
        if( strpos( $test_parts[$i], "{") > -1 ){
          $this->_rest_params[ preg_replace( array('/{/','/}/'), array('',''), $test_parts[$i] ) ] = $rest_parts[ $i ];
        }
      }
      $this->_rest_params['query_string_parameters'] = $this->_query_params;
      
      $controller = $this->_route_match['class'] ;
      if( class_exists( $controller ) ){
        $controller_method = $this->_route_match['callback'];
        $page_controller = new $controller();
        $page_controller ->{$controller_method}( $this->_rest_params );
      }
    }
  }

  private function checkRoutes(){
    $found = false;
    for( $i = 0; $i < count( $this->_route_table ); $i++ ){
      if( preg_match( "/" .  $this->_route_table[$i]['test'] . "$/", $this->_path) ){
        if( $this->checkVerbs( $this->_route_table[$i]['verb'] ) ){
          $this->_route_match = $this->_route_table[$i];
          $found = true;
          break;
        }
      }
    }
    return $found;
  }

  private function checkVerbs( $verbTest ){
    return ( is_array( $verbTest ) ? in_array( strtoupper($this->_verb), array_map('strtoupper',$verbTest) ) : strtoupper($this->_verb)  === strtoupper($verbTest) );
  }

  //public methods 
  public function __construct(){
    spl_autoload_register( 'ep\Router\Utils\AutoLoader::loadClass' );
  }

  static public function loadClass( $this_classname ){
    if( strlen( $this_classname) > 0 ){
      $class_path = "";
      switch( true ){
        case preg_match( '/[a-zA-Z]+Interface$/', $this_classname ) :
          // Load the PHPRest Interface classes from the library
          $class_path = './lib/PHPRest/Interfaces/' .
                        substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) .
                        '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+Abstract$/', $this_classname ) :
          // Load the PHPRest Abstract classes from the library
          $class_path = './lib/PHPRest/Abstracts/' .
                        substr( $this_classname, strrpos( $this_classname, '\\' ) + 1 ) .
                        '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+Controller$/', $this_classname ) :
          // Load any custom Controller classes from the controllers project folder
          $class_path =  './controllers/' . $this_classname . '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+Model$/', $this_classname ) :
          // Load any custom Model classes from the models project folder
          $class_path =  './models/' . $this_classname . '.class.php';
          break;
        case preg_match( '/[a-zA-Z]+View$/', $this_classname ) :
          // Load any custom Views classes from the views project folder
          $class_path =  './views/' . $this_classname . '.class.php';
          break;
      }
      include $class_path;
    }
  }

  public function setRouteTable( $these_routes ){
      for( $i = 0; $i < count( $these_routes ); $i++ ){
        if( !is_null( $these_routes[$i] ) ){
          $route_parts = explode( "/", $these_routes[ $i ]['route'] );
          $regex_route = '^';
          for( $j = 0; $j < count($route_parts); $j++){
            $regex_route .= ( strpos( $route_parts[ $j ], '{' ) > -1 ) ? "\\/[\\w{}'" . '"' .":_%\\-.,]+"  :  "\\/" . $route_parts[ $j ];
          }
          $these_routes[ $i ]['test'] = $regex_route;
        }else{
          $these_routes[ $i ]['test'] = null;
        }
      }
      $this->_route_table = $these_routes;
  }

  public function watch(){

    $this->_verb = $_SERVER[ 'REQUEST_METHOD' ];
    $this->_url = $_SERVER['REQUEST_URI'];
    $this->_path = ( isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "/" );
    $this->_query_string = $_SERVER['QUERY_STRING'];
    
    $this->_view_format = 'json';
    if( isset( $this->_query_params['format'] ) ){
      $this->$_view_format = $this->_query_params['format'];
    }

    $this->parseParams();
  }

}