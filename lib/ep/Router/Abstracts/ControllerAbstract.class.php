<?php

namespace ep\Router\Abstracts;

use ep\Router\Interfaces as Interfaces;

abstract class ControllerAbstract implements Interfaces\ControllerInterface{

  public function __construct(){}

}