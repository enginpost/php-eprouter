<?php

namespace ep\Router\Abstracts;

use ep\Router\Interfaces as Interfaces;

abstract class ViewAbstract implements Interfaces\ViewInterface{
  abstract public function render( $data ); // Extending class must implement an override for this function
}