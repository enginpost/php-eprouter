<?php
include_once('./lib/ep/Router/Utils/AutoLoader.class.php');
include_once(  "./lib/ep/Router/RouteManager.class.php" );

$objRouteManager = new ep\Router\RouteManager();

$routes = array(
    array(
      "route" =>  "", 
      "verb" =>  array( "Get", "PUT", "PATCH", "DELETE" ),
      "class" =>  "ProjectsController",
      "callback" =>  "home"
    ),
    array(
      "route" =>  "pm/{project}/tasks", 
      "verb" =>  "GET",
      "class" =>  "ProjectsController",
      "callback" =>  "tasks"
    ),
    array(
      "route" =>  "pm/{project}/task/{taskid}", 
      "verb" =>  "GET",
      "class" =>  "ProjectsController",
      "callback" =>  "task"
    )
  );

$objRouteManager->setRouteTable( $routes );
$objRouteManager->watch();