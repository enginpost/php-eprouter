<?php

class ProjectsController extends ep\Router\Abstracts\ControllerAbstract{

  private $_controller_vars;
  private $_parameters;
  private $_view_format;

  public function __construct(){}

  public function home( $data ){
    echo "You just called home";
    echo "<pre>" . print_r( $data, true ) . "</pre>";
    if( class_exists('ProjectsHomeView') ){
      $prjHome_view = new ProjectsHomeView();
      $prjHome_view->render( $data );
    }
  }
  public function tasks( $data ){
    echo "You just called tasks";
    echo "<pre>" . print_r( $data, true ) . "</pre>";
  }
  public function task( $data ){
    echo "You just called task";
    echo "<pre>" . print_r( $data, true ) . "</pre>";
  }
  
}